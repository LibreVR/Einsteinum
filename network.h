#pragma once

class Network
{
	int connfd=-1;
	int listenfd=-1;
	public:
	void start();
	int connect();
	bool connected(){return connfd!=-1;}
	void send(void* data,int size=0);
	bool poll();
	void shutdown();
	
};
