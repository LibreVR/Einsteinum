#include "network.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include <fcntl.h>

int guard(int n, char * err) { if (n == -1) { perror(err); exit(1); } return n; }

void Network::start() {
    
    struct sockaddr_in serv_addr; 

    char sendBuff[1025];
    time_t ticks; 

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(9999); 

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

    listen(listenfd, 10); 
    
    int flags = guard(fcntl(listenfd, F_GETFL), (char*)"could not get file flags");
	guard(fcntl(listenfd, F_SETFL, flags | O_NONBLOCK), (char*)"could not set file flags");
    
}

int Network::connect()
{
	const char* sendBuff=(const char*)"Einsteinium VR prototype\n";
	connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
	if(connfd>0){
		write(connfd, sendBuff, strlen(sendBuff)); 
		int flags = guard(fcntl(connfd, F_GETFL), (char*)"could not get file flags");
		guard(fcntl(connfd, F_SETFL, flags | O_NONBLOCK), (char*)"could not set file flags");
	}
	
	return connfd;
}

void Network::shutdown()
{
	close(listenfd);
	if(connfd>0) close(connfd);
}

void Network::send(void* data,int size)
{
	if(size==0) size=strlen((char*)data);
	write(connfd,data,size);
}

bool Network::poll()
{
	char buffer[1024];
	int size=1023;
	int count = read(connfd,buffer,size);
	if(count>0)
	{
		buffer[count]=0;
		printf("Network::poll() size=%i data=%s\n",count,buffer);
		//TODO: store data somewhere
		return true;
	}
	return false;
}
