#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "network.h"

int display_h = 720;
int display_w = 1440;

int frames=0;
int fps=0;
SDL_Window * screen=nullptr;

//TODO: add networking code - not using sdl here

Uint32 count_fps(Uint32 interval, void *param)
{
	
	char title[128];
	sprintf(title,"Einsteinium VR fps=%i\n",frames);
	SDL_SetWindowTitle(screen,title);
	if(fps==0) fps=frames;
	else fps = (fps+frames)/2;
	frames=0;
	return interval;
}

volatile bool running=true;
Network network;
//USBConn usb;
 
int main(int argc, char ** argv)
{
    volatile bool quit = false;
    
    SDL_Event event;
 
    SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_JPG);
    network.start();
 
    screen = SDL_CreateWindow("Einsteinium VR", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, display_w,display_h,0);    
    SDL_Renderer * renderer = SDL_CreateRenderer(screen, -1, 0);
    SDL_AddTimer(1000,count_fps,nullptr);


    while (!quit)
    {
		if(!network.connected())
		{
			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			int fd = network.connect();
			if(fd>0)
			{
				char screen[1024];
				sprintf(screen,"screen %i %i\n",display_w,display_h);
				network.send((char*)screen);
			}
		}
		else
		{
			SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
			if(network.poll())
			{
				//TODO: handle network event
			}
		}
        SDL_RenderClear(renderer);
		SDL_RenderPresent(renderer);
		
		frames++;
		
        SDL_PollEvent(&event);
       
        
        switch (event.type)
        {
            case SDL_QUIT:
                quit = true;
                break;
        }
    }
    
    network.shutdown();
 
    SDL_Quit();
 
    return 0;
}
